package com.gna.uw3pro.Infrastructure;

import com.gna.uw3pro.R;

import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class BaseDisplayExerciseClass extends CommonBaseClass {
	protected String[] exerciseTextArray;
	protected String[] exerciseNameArray;
	protected int[] firstPictureArray;
	protected int[] secondPictureArray;
	protected int[] thirdPictureArray;
	protected int[] imageCount;

	protected ImageView firstImage;
	protected ImageView secondImage;
	protected ImageButton rightArrowBtn;
	protected ImageButton leftArrowBtn;
	protected ImageButton cancelBtn;
	protected TextView exerciseText;
	protected TextView exerciseCount;
	protected TextView exerciseName;
	protected ImageView titleView;
	protected ImageButton infoBtn;

	public void intializeViews() {
		firstImage = (ImageView) findViewById(R.id.firstImage);
		secondImage = (ImageView) findViewById(R.id.secondImage);
		rightArrowBtn = (ImageButton) findViewById(R.id.rightArrowBtn);
		leftArrowBtn = (ImageButton) findViewById(R.id.leftArrowBtn);
		exerciseText = (TextView) findViewById(R.id.exerciseText);
		exerciseCount = (TextView) findViewById(R.id.exerciseCount);
		exerciseName = (TextView) findViewById(R.id.exerciseName);
		titleView = (ImageView) findViewById(R.id.titleView);
		cancelBtn = (ImageButton) findViewById(R.id.cancelBtn);
		infoBtn = (ImageButton) findViewById(R.id.infoBtn);

	}

	public void workoutAExercise() {
		titleView.setBackgroundResource(R.drawable.title01);
		exerciseTextArray = new String[8];
		exerciseNameArray = new String[8];
		firstPictureArray = new int[8];
		secondPictureArray = new int[8];
		thirdPictureArray = new int[8];
		imageCount = new int[8];

		exerciseTextArray[0] = "With feet elevated on a bench or other similar object, perform a strict push up.";
		exerciseTextArray[1] = "Perform a BW Squat, make sure your heels stay on the ground and your legs go parallel.";
		exerciseTextArray[2] = "With hands close together under shoulders, in a push up position.  Begin the move, keeping elbows tight to your side.  Drop to knees if this is too hard.";
		exerciseTextArray[3] = "Begin in a slow jog but quickly get your knees high so every time you raise your legs in the stationary run your knees are coming up parallel.";
		exerciseTextArray[4] = "Standing, lean forward and reach outward with your left hand as your right leg kicks back so you end with your body horizontal, control the balance and then do all the reps on the other side";
		exerciseTextArray[5] = "With any grip you prefer, perform a pull up, squeezing your biceps at the top point and control descent.";
		exerciseTextArray[6] = "On your back, bend one knee and straighten the other leg holding knees close together, now use your leg as an aid to climb it with your hands as you crunch upward and finally reach to tap your toe, return slowly and repeat all reps on this side, then do all reps on the other side.  Don't lower your leg.";
		exerciseTextArray[7] = "Similar to sky pointers except this time have the soles of your feet together,knees split.Keep this position as you then raise your legs, lift your hips, then return to start and repeat.";

		exerciseNameArray[0] = "Elevated Push Ups";
		exerciseNameArray[1] = "BW Squats";
		exerciseNameArray[2] = "Close Grip Push Up";
		exerciseNameArray[3] = "High Knees";
		exerciseNameArray[4] = "Balance Lean";
		exerciseNameArray[5] = "Pull Ups";
		exerciseNameArray[6] = "Leg Climber";
		exerciseNameArray[7] = "Split Frogs";

		firstPictureArray[0] = R.drawable.w1_exercise01_01;
		firstPictureArray[1] = R.drawable.w1_exercise02_01;
		firstPictureArray[2] = R.drawable.w1_exercise03_01;
		firstPictureArray[3] = R.drawable.w1_exercise04_01;
		firstPictureArray[4] = R.drawable.w1_exercise05_01;
		firstPictureArray[5] = R.drawable.w1_exercise06_01;
		firstPictureArray[6] = R.drawable.w1_exercise07_01;
		firstPictureArray[7] = R.drawable.w1_exercise08_01;

		secondPictureArray[0] = R.drawable.w1_exercise01_02;
		secondPictureArray[1] = R.drawable.w1_exercise02_02;
		secondPictureArray[2] = R.drawable.w1_exercise03_02;
		secondPictureArray[3] = R.drawable.w1_exercise04_02;
		secondPictureArray[4] = R.drawable.w1_exercise05_02;
		secondPictureArray[5] = R.drawable.w1_exercise06_02;
		secondPictureArray[6] = R.drawable.w1_exercise07_02;
		secondPictureArray[7] = R.drawable.w1_exercise08_02;

		thirdPictureArray[0] = 0;
		thirdPictureArray[1] = 0;
		thirdPictureArray[2] = 0;
		thirdPictureArray[3] = 0;
		thirdPictureArray[4] = 0;
		thirdPictureArray[5] = 0;
		thirdPictureArray[6] = 0;
		thirdPictureArray[7] = 0;

		imageCount[0] = 2;
		imageCount[1] = 2;
		imageCount[2] = 2;
		imageCount[3] = 2;
		imageCount[4] = 2;
		imageCount[5] = 2;
		imageCount[6] = 2;
		imageCount[7] = 2;

	}

	public void workoutBExercise() {
		titleView.setBackgroundResource(R.drawable.title02);
		exerciseTextArray = new String[4];
		exerciseNameArray = new String[4];
		firstPictureArray = new int[4];
		secondPictureArray = new int[4];
		thirdPictureArray = new int[4];
		imageCount = new int[4];

		exerciseTextArray[0] = "Grabbing an over head bar with a wide grip etc, perform a pull up, pause at the top then lower slowly.";
		exerciseTextArray[1] = "From standing, bend and lean forward to place palms on the floor.  'Walk' out on hands four paces, do a push up, 'walk' back ward to start position and repeat.";
		exerciseTextArray[2] = "Begin in a push up position, then twist and raise your arm vertical to hold a T position, with feet stacked.  Don't wobble too much.  Then return to start - perform optional push up - and repeat the other side.";
		exerciseTextArray[3] = "In a squat position, squat down legs parallel, then explode up and jump, land softly and repeat.";

		exerciseNameArray[0] = "Wide Pull Up";
		exerciseNameArray[1] = "Spiderman walk Outs";
		exerciseNameArray[2] = "T Bar Twists";
		exerciseNameArray[3] = "BW Squat Jumps";

		firstPictureArray[0] = R.drawable.w2_exercise01_01;
		firstPictureArray[1] = R.drawable.w2_exercise02_01;
		firstPictureArray[2] = R.drawable.w2_exercise03_01;
		firstPictureArray[3] = R.drawable.w2_exercise04_01;

		secondPictureArray[0] = R.drawable.w2_exercise01_02;
		secondPictureArray[1] = R.drawable.w2_exercise02_02;
		secondPictureArray[2] = R.drawable.w2_exercise03_02;
		secondPictureArray[3] = R.drawable.w2_exercise04_02;

		thirdPictureArray[0] = 0;
		thirdPictureArray[1] = 0;
		thirdPictureArray[2] = 0;
		thirdPictureArray[3] = 0;

		imageCount[0] = 2;
		imageCount[1] = 2;
		imageCount[2] = 2;
		imageCount[3] = 2;

	}

	public void workoutCExercise() {
		titleView.setBackgroundResource(R.drawable.title03);
		exerciseTextArray = new String[6];
		exerciseNameArray = new String[6];
		firstPictureArray = new int[6];
		secondPictureArray = new int[6];
		thirdPictureArray = new int[6];
		imageCount = new int[6];

		exerciseTextArray[0] = "By the pull up bar perform 2 squats or squat jumpers (your choice) then immediately grab the pull up bar and perform 2-3 pull ups.  then back to the squats and so on...";
		exerciseTextArray[1] = "With palms flat perform the push up.  Go on your knees if needed.";
		exerciseTextArray[2] = "Get ready to do a lunge, make sure knee is at 90 degrees as you lunge diagonally and raise your arms high and vertical.  Repeat the other side.";
		exerciseTextArray[3] = "In your push up position:  move your left knee out and up toward your ear, tensing your core, repeat the other side, THEN back to the left leg: swing your knee under your body to the other hip, hold then return and repeat the other side. This is all 1 rep. Repeat this pattern for the next rep and so on...";
		exerciseTextArray[4] = "With feet elevated on a bench, place your hands carefully down on the floor, closer to the bench than a standard push up. Now lower your head slowly down to the hands and up, this is a shoulder dip pres up. If too hard you can stand on floor to do this.";
		exerciseTextArray[5] = "On your back, raise your legs vertical.  Crunch your lower abs and lift your hips off the floor, pointing your feet toward the sky.  Don't let legs wobble. Repeat.";

		exerciseNameArray[0] = "Double Squat / Double Pull Up";
		exerciseNameArray[1] = "Push Up";
		exerciseNameArray[2] = "Diagonal Arm Raise Lunge";
		exerciseNameArray[3] = "Knee Out Unders";
		exerciseNameArray[4] = "Shoulder Elevated Push Ups";
		exerciseNameArray[5] = "Sky Pointers";

		firstPictureArray[0] = R.drawable.w3_exercise01_01;
		firstPictureArray[1] = R.drawable.w3_exercise02_01;
		firstPictureArray[2] = R.drawable.w3_exercise03_01;
		firstPictureArray[3] = R.drawable.w3_exercise04_01;
		firstPictureArray[4] = R.drawable.w3_exercise05_01;
		firstPictureArray[5] = R.drawable.w3_exercise06_01;

		secondPictureArray[0] = R.drawable.w3_exercise01_02;
		secondPictureArray[1] = R.drawable.w3_exercise02_02;
		secondPictureArray[2] = R.drawable.w3_exercise03_02;
		secondPictureArray[3] = R.drawable.w3_exercise04_02;
		secondPictureArray[4] = R.drawable.w3_exercise05_02;
		secondPictureArray[5] = R.drawable.w3_exercise06_02;

		thirdPictureArray[0] = 0;
		thirdPictureArray[1] = 0;
		thirdPictureArray[2] = 0;
		thirdPictureArray[3] = 0;
		thirdPictureArray[4] = 0;
		thirdPictureArray[5] = 0;

		imageCount[0] = 2;
		imageCount[1] = 2;
		imageCount[2] = 2;
		imageCount[3] = 2;
		imageCount[4] = 2;
		imageCount[5] = 2;

	}

	public void workoutDExercise() {
		titleView.setBackgroundResource(R.drawable.title04);
		exerciseTextArray = new String[6];
		exerciseNameArray = new String[6];
		firstPictureArray = new int[6];
		secondPictureArray = new int[6];
		thirdPictureArray = new int[6];
		imageCount = new int[6];

		exerciseTextArray[0] = "Set 2 cones around 20-40 yards apart and begin to sprint  or side shuffle to the other cones, get low, stop and explode back the other direction.";
		exerciseTextArray[1] = "Perform a BW Squat with ballistic jump, make sure your heels stay on the ground and your legs go parallel.";
		exerciseTextArray[2] = "This is a standard push up, except as you rise each time let your leg raise up like a scissor and down as you come down on the push up.  Change to opposite leg next rep.";
		exerciseTextArray[3] = "Set 2 cones around 20-40 yards apart and begin to hop on one leg to the other cones, turn, and head back on other leg.";
		exerciseTextArray[4] = "On your back perform a crunch and reach your right foot with left hand, then repeat the opposite side.";
		exerciseTextArray[5] = "In the plank position, hold the move completely static.";

		exerciseNameArray[0] = "Point 2 Point Run";
		exerciseNameArray[1] = "BW Jump Squat";
		exerciseNameArray[2] = "Scissor Push Up";
		exerciseNameArray[3] = "Point 2 Point Hops";
		exerciseNameArray[4] = "Alt.Foot 2 Toe Crunch";
		exerciseNameArray[5] = "Plank";

		firstPictureArray[0] = R.drawable.w4_exercise01_01;
		firstPictureArray[1] = R.drawable.w4_exercise02_01;
		firstPictureArray[2] = R.drawable.w4_exercise03_01;
		firstPictureArray[3] = R.drawable.w4_exercise04_01;
		firstPictureArray[4] = R.drawable.w4_exercise05_01;
		firstPictureArray[5] = R.drawable.w4_exercise06_01;

		secondPictureArray[0] = R.drawable.w4_exercise01_02;
		secondPictureArray[1] = R.drawable.w4_exercise02_02;
		secondPictureArray[2] = R.drawable.w4_exercise03_02;
		secondPictureArray[3] = R.drawable.w4_exercise04_02;
		secondPictureArray[4] = R.drawable.w4_exercise05_02;
		secondPictureArray[5] = 0;

		thirdPictureArray[0] = 0;
		thirdPictureArray[1] = 0;
		thirdPictureArray[2] = 0;
		thirdPictureArray[3] = 0;
		thirdPictureArray[4] = 0;
		thirdPictureArray[5] = 0;

		imageCount[0] = 2;
		imageCount[1] = 2;
		imageCount[2] = 2;
		imageCount[3] = 2;
		imageCount[4] = 2;
		imageCount[5] = 1;

	}

	public void workoutEExercise() {
		titleView.setBackgroundResource(R.drawable.title05);
		exerciseTextArray = new String[7];
		exerciseNameArray = new String[7];
		firstPictureArray = new int[7];
		secondPictureArray = new int[7];
		thirdPictureArray = new int[7];
		imageCount = new int[7];

		exerciseTextArray[0] = "This is a standard plank position, except as you hold the move let your leg raise up like a scissor and down.  Change to opposite leg in alternating sequence.";
		exerciseTextArray[1] = "Perform a BW squat, but after 2 squats each side jump and twist 180 degrees so you land and do 2 squats facing the other way.  repeat.  (Change direction of the twist jump so you don't get dizzy)";
		exerciseTextArray[2] = "Begin in a slow jog but quickly get your knees high so every time you raise your legs in the stationary run your knees are coming up parallel.";
		exerciseTextArray[3] = "Perform a push up with feet elevated.";
		exerciseTextArray[4] = "Set 2 cones around 20-40 yards apart and begin to sprint  or side shuffle to the other cones, get low, stop and explode back the other direction.";
		exerciseTextArray[5] = "In a plank position, begin to raise up on your right arm so its straight, then your left, then down on your right to start position, then your left.  Repeat the pattern for the next rep.";
		exerciseTextArray[6] = "In a standard push up position, begin a flowing movement where you arch your hips high, sucking in your abs hard. Then lower your hips as you look skyward, repeat.";

		exerciseNameArray[0] = "Plank Scissors";
		exerciseNameArray[1] = "BW 180 Squat Jumps";
		exerciseNameArray[2] = "High Knees";
		exerciseNameArray[3] = "Elevated Push Up";
		exerciseNameArray[4] = "Point 2 Point Runs";
		exerciseNameArray[5] = "Plank Climber";
		exerciseNameArray[6] = "Arches";

		firstPictureArray[0] = R.drawable.w5_exercise01_01;
		firstPictureArray[1] = R.drawable.w5_exercise02_01;
		firstPictureArray[2] = R.drawable.w5_exercise03_01;
		firstPictureArray[3] = R.drawable.w5_exercise04_01;
		firstPictureArray[4] = R.drawable.w5_exercise05_01;
		firstPictureArray[5] = R.drawable.w5_exercise06_01;
		firstPictureArray[6] = R.drawable.w5_exercise07_01;

		secondPictureArray[0] = R.drawable.w5_exercise01_02;
		secondPictureArray[1] = R.drawable.w5_exercise02_02;
		secondPictureArray[2] = R.drawable.w5_exercise03_02;
		secondPictureArray[3] = R.drawable.w5_exercise04_02;
		secondPictureArray[4] = R.drawable.w5_exercise05_02;
		secondPictureArray[5] = R.drawable.w5_exercise06_02;
		secondPictureArray[6] = R.drawable.w5_exercise07_02;

		thirdPictureArray[0] = 0;
		thirdPictureArray[1] = 0;
		thirdPictureArray[2] = 0;
		thirdPictureArray[3] = 0;
		thirdPictureArray[4] = 0;
		thirdPictureArray[5] = 0;
		thirdPictureArray[6] = 0;

		imageCount[0] = 2;
		imageCount[1] = 2;
		imageCount[2] = 2;
		imageCount[3] = 2;
		imageCount[4] = 2;
		imageCount[5] = 2;
		imageCount[6] = 2;

	}

}
