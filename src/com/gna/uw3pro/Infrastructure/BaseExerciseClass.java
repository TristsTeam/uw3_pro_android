package com.gna.uw3pro.Infrastructure;

import com.gna.uw3pro.R;

import android.graphics.Typeface;
import android.graphics.drawable.StateListDrawable;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class BaseExerciseClass extends CommonBaseClass {

	protected ImageButton leftArrowBtn;
	protected ImageButton infoBtn;
	protected TextView displayExercise1;
	protected TextView displayExercise2;
	protected TextView displayExercise3;
	protected TextView displayExercise4;
	protected TextView displayExercise5;
	protected TextView displayExercise6;
	protected TextView displayExercise7;
	protected TextView displayExercise8;
	protected ImageButton rightArrowBtn1;
	protected ImageButton rightArrowBtn2;
	protected ImageButton rightArrowBtn3;
	protected ImageButton rightArrowBtn4;
	protected ImageButton rightArrowBtn5;
	protected ImageButton rightArrowBtn6;
	protected ImageButton rightArrowBtn7;
	protected ImageButton rightArrowBtn8;

	ImageView titleImage;
	ImageView preExerCiseImage;
	LinearLayout display8parent;
	LinearLayout display7parent;
	LinearLayout display6parent;
	LinearLayout display5parent;
	LinearLayout display4parent;
	LinearLayout rightArrowBtn8Parent;
	LinearLayout rightArrowBtn7Parent;
	LinearLayout rightArrowBtn6Parent;
	LinearLayout rightArrowBtn5Parent;
	LinearLayout rightArrowBtn4Parent;

	public void intializeView() {
		leftArrowBtn = (ImageButton) findViewById(R.id.leftArrowBtn);
		infoBtn = (ImageButton) findViewById(R.id.infoBtn);
		titleImage = (ImageView) findViewById(R.id.titleImage);
		preExerCiseImage = (ImageView) findViewById(R.id.preExerciseImage);
		display8parent = (LinearLayout) findViewById(R.id.displayExercise8Parent);
		display7parent = (LinearLayout) findViewById(R.id.displayExercise7Parent);
		display6parent = (LinearLayout) findViewById(R.id.displayExercise6Parent);
		display5parent = (LinearLayout) findViewById(R.id.displayExercise5Parent);
		display4parent = (LinearLayout) findViewById(R.id.displayExercise4Parent);

		rightArrowBtn8Parent = (LinearLayout) findViewById(R.id.rightArrowBtn8Parent);
		rightArrowBtn7Parent = (LinearLayout) findViewById(R.id.rightArrowBtn7Parent);
		rightArrowBtn6Parent = (LinearLayout) findViewById(R.id.rightArrowBtn6Parent);
		rightArrowBtn5Parent = (LinearLayout) findViewById(R.id.rightArrowBtn5Parent);
		rightArrowBtn4Parent = (LinearLayout) findViewById(R.id.rightArrowBtn4Parent);

		StateListDrawable infoBtnStates = new StateListDrawable();
		infoBtnStates.addState(new int[] { android.R.attr.state_pressed }, this
				.getResources().getDrawable(R.drawable.infobar_down));
		infoBtnStates.addState(new int[] {},
				this.getResources().getDrawable(R.drawable.infobar_norm));
		infoBtn.setBackgroundDrawable(infoBtnStates);

		displayExercise1 = (TextView) findViewById(R.id.displayExercise1);
		displayExercise2 = (TextView) findViewById(R.id.displayExercise2);
		displayExercise3 = (TextView) findViewById(R.id.displayExercise3);
		displayExercise4 = (TextView) findViewById(R.id.displayExercise4);
		displayExercise5 = (TextView) findViewById(R.id.displayExercise5);
		displayExercise6 = (TextView) findViewById(R.id.displayExercise6);
		displayExercise7 = (TextView) findViewById(R.id.displayExercise7);
		displayExercise8 = (TextView) findViewById(R.id.displayExercise8);

		rightArrowBtn1 = (ImageButton) findViewById(R.id.rightArrowBtn1);
		rightArrowBtn2 = (ImageButton) findViewById(R.id.rightArrowBtn2);
		rightArrowBtn3 = (ImageButton) findViewById(R.id.rightArrowBtn3);
		rightArrowBtn4 = (ImageButton) findViewById(R.id.rightArrowBtn4);
		rightArrowBtn5 = (ImageButton) findViewById(R.id.rightArrowBtn5);
		rightArrowBtn6 = (ImageButton) findViewById(R.id.rightArrowBtn6);
		rightArrowBtn7 = (ImageButton) findViewById(R.id.rightArrowBtn7);
		rightArrowBtn8 = (ImageButton) findViewById(R.id.rightArrowBtn8);

		Typeface koratakiRgTypeFace = Typeface.createFromAsset(getAssets(),
				"fonts/KoratakiRg.ttf");
		displayExercise1.setTypeface(koratakiRgTypeFace);
		displayExercise2.setTypeface(koratakiRgTypeFace);
		displayExercise3.setTypeface(koratakiRgTypeFace);
		displayExercise4.setTypeface(koratakiRgTypeFace);
		displayExercise5.setTypeface(koratakiRgTypeFace);
		displayExercise6.setTypeface(koratakiRgTypeFace);
		displayExercise7.setTypeface(koratakiRgTypeFace);
		displayExercise8.setTypeface(koratakiRgTypeFace);

	}

	public void bindWorkoutA() {
		titleImage.setBackgroundResource(R.drawable.title01);
		preExerCiseImage.setBackgroundResource(R.drawable.preexercises_01);
		displayExercise1.setText("Elevated Push Ups");
		displayExercise2.setText("BW Squats");
		displayExercise3.setText("Close Grip Push Up");
		displayExercise4.setText("High Knees");
		displayExercise5.setText("Balance Lean");
		displayExercise6.setText("Pull Ups");
		displayExercise7.setText("Leg Climber");
		displayExercise8.setText("Split Frogs");
	}

	public void bindWorkoutB() {
		display8parent.setVisibility(View.GONE);
		rightArrowBtn8Parent.setVisibility(View.GONE);
		display7parent.setVisibility(View.GONE);
		rightArrowBtn7Parent.setVisibility(View.GONE);
		display6parent.setVisibility(View.GONE);
		rightArrowBtn6Parent.setVisibility(View.GONE);
		display5parent.setVisibility(View.GONE);
		rightArrowBtn5Parent.setVisibility(View.GONE);

		titleImage.setBackgroundResource(R.drawable.title02);
		preExerCiseImage.setBackgroundResource(R.drawable.preexercises_02);
		displayExercise1.setText("Wide Pull Up");
		displayExercise2.setText("Spiderman walk Outs");
		displayExercise3.setText("T Bar Twists");
		displayExercise4.setText("BW Squat Jumps");

	}

	public void bindWorkoutC() {
		display8parent.setVisibility(View.GONE);
		rightArrowBtn8Parent.setVisibility(View.GONE);

		display7parent.setVisibility(View.GONE);
		rightArrowBtn7Parent.setVisibility(View.GONE);
		titleImage.setBackgroundResource(R.drawable.title03);
		preExerCiseImage.setBackgroundResource(R.drawable.preexercises_03);
		displayExercise1.setText("Double Squat / Double Pull Up");
		displayExercise2.setText("Push Up");
		displayExercise3.setText("Diagonal Arm Raise Lunge");
		displayExercise4.setText("Knee Out Unders");
		displayExercise5.setText("Shoulder Elevated Push Ups");
		displayExercise6.setText("Sky Pointers");

	}

	public void bindWorkoutD() {
		display8parent.setVisibility(View.GONE);
		rightArrowBtn8Parent.setVisibility(View.GONE);

		display7parent.setVisibility(View.GONE);
		rightArrowBtn7Parent.setVisibility(View.GONE);
		titleImage.setBackgroundResource(R.drawable.title04);
		preExerCiseImage.setBackgroundResource(R.drawable.preexercises_04);
		displayExercise1.setText("Point 2 Point Run");
		displayExercise2.setText("BW Jump Squat");
		displayExercise3.setText("Scissor Push Up");
		displayExercise4.setText("Point 2 Point Hops");
		displayExercise5.setText("Alt.Foot 2 Toe Crunch");
		displayExercise6.setText("Plank");
	}

	public void bindWorkoutE() {
		display8parent.setVisibility(View.GONE);
		rightArrowBtn8Parent.setVisibility(View.GONE);
		titleImage.setBackgroundResource(R.drawable.title05);
		preExerCiseImage.setBackgroundResource(R.drawable.preexercises_05);
		displayExercise1.setText("Plank Scissors");
		displayExercise2.setText("BW 180 Squat Jumps");
		displayExercise3.setText("High Knees");
		displayExercise4.setText("Elevated Push Up");
		displayExercise5.setText("Point 2 Point Runs");
		displayExercise6.setText("Plank Climber");
		displayExercise7.setText("Arches");
	}

}
