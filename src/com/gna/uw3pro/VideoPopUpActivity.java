package com.gna.uw3pro;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.gna.uw3pro.Infrastructure.KeepScreenOnBaseClass;

/**
 * Created by rohitgarg on 6/3/13.
 */
public class VideoPopUpActivity extends KeepScreenOnBaseClass implements View.OnClickListener {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.video_popup);
        keepScreenOn();

        ImageView leftArrowClick=(ImageView)findViewById(R.id.crossBtn);
        leftArrowClick.setOnClickListener(this);

        LinearLayout menClick=(LinearLayout)findViewById(R.id.menClick);
        LinearLayout womenClick=(LinearLayout)findViewById(R.id.womenClick);

        menClick.setOnClickListener(this);
        womenClick.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view.getId()==R.id.crossBtn)
        {
            this.finish();
        }else if(view.getId()==R.id.menClick)
        {
            Intent myWebLink = new Intent(Intent.ACTION_VIEW);
            myWebLink
                    .setData(Uri
                            .parse("http://UKNOMAD.mikegeary1.hop.clickbank.net/?pid=466"));
            startActivity(myWebLink);
        }
        else if(view.getId()==R.id.womenClick)
        {
            Intent myWebLink = new Intent(Intent.ACTION_VIEW);
            myWebLink
                    .setData(Uri
                            .parse("http://UKNOMAD.mikegeary1.hop.clickbank.net/?pid=465"));
            startActivity(myWebLink);
        }
    }
}
