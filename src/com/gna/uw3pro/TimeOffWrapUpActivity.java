package com.gna.uw3pro;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gna.uw3pro.Infrastructure.BaseExerciseClass;

public class TimeOffWrapUpActivity extends BaseExerciseClass implements
		OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		if (getIntent().getExtras().getInt("value") == 2)
			setContentView(R.layout.workout2);
		else
			setContentView(R.layout.workout);
		intializeView();
		if (getIntent().getExtras().getInt("value") == 1)
			bindWorkoutA();
		else if (getIntent().getExtras().getInt("value") == 2)
			bindWorkoutB();
		else if (getIntent().getExtras().getInt("value") == 3)
			bindWorkoutC();
		else if (getIntent().getExtras().getInt("value") == 4)
			bindWorkoutD();
		else if (getIntent().getExtras().getInt("value") == 5)
			bindWorkoutE();

		intializeStates();
		intializeClickListener();
		keepScreenOn();

		LinearLayout startBtnLayout = (LinearLayout) findViewById(R.id.startBtnLayout);
		startBtnLayout.setVisibility(View.GONE);

		// Typeface helveticeTypeFace = Typeface.createFromAsset(getAssets(),
		// "fonts/Helvetica.ttf");
		TextView startLeftTextView = (TextView) findViewById(R.id.startLeftTextView);
		// startLeftTextView.setTypeface(helveticeTypeFace);

	}

	private void intializeClickListener() {
		emailButton.setOnClickListener(this);
		musicButton.setOnClickListener(this);
		leftArrowBtn.setOnClickListener(this);
		infoBtn.setOnClickListener(this);
		notesBarBtn.setOnClickListener(this);

		displayExercise1.setOnClickListener(this);
		displayExercise2.setOnClickListener(this);
		displayExercise3.setOnClickListener(this);
		displayExercise4.setOnClickListener(this);
		displayExercise5.setOnClickListener(this);
		displayExercise6.setOnClickListener(this);
		displayExercise7.setOnClickListener(this);
		displayExercise8.setOnClickListener(this);

		rightArrowBtn1.setOnClickListener(this);
		rightArrowBtn2.setOnClickListener(this);
		rightArrowBtn3.setOnClickListener(this);
		rightArrowBtn4.setOnClickListener(this);
		rightArrowBtn5.setOnClickListener(this);
		rightArrowBtn6.setOnClickListener(this);
		rightArrowBtn7.setOnClickListener(this);
		rightArrowBtn8.setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v.getId() == R.id.emailBarBtn) {
			email();
		} else if (v.getId() == R.id.musicBarBtn) {
			try {
				Intent intent = new Intent(android.content.Intent.ACTION_MAIN);
				intent.addCategory("android.intent.category.APP_MUSIC");
				startActivity(intent);
			} catch (ActivityNotFoundException e) {
				Intent intent = new Intent("android.intent.action.MUSIC_PLAYER");
				startActivity(intent);
			}
		} else if (v.getId() == R.id.leftArrowBtn) {
			Intent intent = new Intent(this, NoofWrapsActvity.class);
			intent.putExtra("isTimedMode", false);
			intent.putExtra("value", getIntent().getExtras().getInt("value"));
			intent.putExtra("comingFrom", "front");
			startActivity(intent);
			this.finish();
		} else if (v.getId() == R.id.infoBtn) {
			Intent intent = new Intent(this, NoofWrapsActvity.class);
			intent.putExtra("value", getIntent().getExtras().getInt("value"));
			intent.putExtra("comingFrom", "back");
			startActivity(intent);
		} else if (v.getId() == R.id.notesBarBtn) {
			Intent intent = new Intent(this, NotesActivity.class);
			startActivity(intent);
		} else {
			Intent intent = new Intent(this, DisplayExerciseActivity.class);
			intent.putExtra("value", getIntent().getExtras().getInt("value"));
			intent.putExtra("isTimeMode", false);
			if (v.getId() == R.id.displayExercise1
					|| v.getId() == R.id.rightArrowBtn1) {
				intent.putExtra("selectedValue", 0);
			} else if (v.getId() == R.id.displayExercise2
					|| v.getId() == R.id.rightArrowBtn2) {
				intent.putExtra("selectedValue", 1);
			} else if (v.getId() == R.id.displayExercise3
					|| v.getId() == R.id.rightArrowBtn3) {
				intent.putExtra("selectedValue", 2);
			} else if (v.getId() == R.id.displayExercise4
					|| v.getId() == R.id.rightArrowBtn4) {
				intent.putExtra("selectedValue", 3);
			} else if (v.getId() == R.id.displayExercise5
					|| v.getId() == R.id.rightArrowBtn5) {
				intent.putExtra("selectedValue", 4);
			} else if (v.getId() == R.id.displayExercise6
					|| v.getId() == R.id.rightArrowBtn6) {
				intent.putExtra("selectedValue", 5);
			} else if (v.getId() == R.id.displayExercise7
					|| v.getId() == R.id.rightArrowBtn7) {
				intent.putExtra("selectedValue", 6);
			} else if (v.getId() == R.id.displayExercise8
					|| v.getId() == R.id.rightArrowBtn8) {
				intent.putExtra("selectedValue", 7);
			}
			startActivity(intent);
			this.finish();
		}
	}

	@Override
	public void onBackPressed() {
		Intent intent = new Intent(this, NoofWrapsActvity.class);
		intent.putExtra("isTimedMode", false);
		intent.putExtra("value", getIntent().getExtras().getInt("value"));
		intent.putExtra("comingFrom", "front");
		startActivity(intent);
		this.finish();
	}

}
