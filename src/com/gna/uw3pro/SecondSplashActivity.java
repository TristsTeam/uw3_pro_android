package com.gna.uw3pro;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.TextView;

import com.gna.uw3pro.Infrastructure.AppRater;
import com.gna.uw3pro.Infrastructure.KeepScreenOnBaseClass;

public class SecondSplashActivity extends KeepScreenOnBaseClass implements
		OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.second_splash);
		keepScreenOn();
		Typeface helveticeTypeFace = Typeface.createFromAsset(getAssets(),
				"fonts/Helvetica.ttf");
		TextView splashMainText = (TextView) findViewById(R.id.splashMainText);
		splashMainText.setTypeface(helveticeTypeFace);

		ImageButton arrowClick = (ImageButton) findViewById(R.id.arrowClick);
		arrowClick.setOnClickListener(this);
		new AppRater().app_launched(this);
	}

	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub
		if (view.getId() == R.id.arrowClick) {
			Intent intent = new Intent(this, JoinUsDialogActivity.class);
            intent.putExtra("isComingFromSecond",true);
			startActivity(intent);
			this.finish();
		}

	}

}
